﻿using System;
using System.Collections.Generic;
using System.Configuration;

public class Main
{
    public int intYamato = 0;
    public int intSagawa = 1;
    public int intSeino = 2;

    public Main()
    {
        // コンフィグファイル読み込み
        List<string> config = ConfigurationSettings.AppSettings.AllKeys;
        string strInputCsv = config["InputCSV"];
        string strYamatoCsv = config["YamatoCSV"];
        string strSagawaCsv = config["SagawaCSV"];
        string strSeinoCsv = config["SeinoCSV"];
        List<string> listYamato = new List<string>();
        List<string> listSagawa = new List<string>();
        List<string> listSeino = new List<string>();

        // 読み込みたいCSVファイルのパスを指定して開く
        StreamReader sr = new StreamReader(strInputCsv);
        {
            // 末尾まで繰り返す
            while (!sr.EndOfStream)
            {
                // CSVファイルの一行を読み込む
                string line = sr.ReadLine();
                // 読み込んだ一行をカンマ毎に分けて配列に格納する
                string[] values = line.Split(',');

                // 運送会社の選択用の値でデータを振り分ける ※TODO 選択用の値はメイプル様に確認する
                if (values[0] == intYamato)
                {
                    // ヤマトの場合
                    listYamato.AddRange(values);
                }
                else if (values[0] == intSagawa)
                {
                    // 佐川の場合
                    listSagawa.AddRange(values);
                }
                else if (values[0] == intSeino)
                {
                    // 西濃の場合
                    listSeino.AddRange(values);
                }
            }
        }
        // 配列を各運送会社の外部データの形に成型する
        // ヤマト
        string[] yamatoCSV = convertCSV(listYamato, intYamato);
        // 佐川
        string[] sagawaCSV = convertCSV(listSagawa, intSagawa);
        // 西濃
        string[] seinoCSV = convertCSV(listSeino, intSeino);

        // CSVファイル作成
        // ヤマト
        StreamWriter fileYamato = new StreamWriter(strYamatoCsv, false, Encoding.Unicode);
        foreach (string lineYamato in yamatoCSV)
        {
            fileYamato.WriteLine(lineYamato); // データ部出力
        }
        fileYamato.Close();

        // 佐川
        StreamWriter fileSagawa = new StreamWriter(strSagawaCsv, false, Encoding.Unicode);
        foreach (string lineSagawa in sagawaCSV)
        {
            fileSagawa.WriteLine(lineSagawa); // データ部出力
        }
        fileSagawa.Close();

        // 西濃
        StreamWriter fileSeino = new StreamWriter(strSeinoCsv, false, Encoding.Unicode);
        foreach (string lineSeino in seinoCSV)
        {
            fileSeino.WriteLine(lineSeino); // データ部出力
        }
        fileSeino.Close();
    }

    // 外部データの形に配列を成型
    public string[] convertCSV(string[] values, int type)
    {
        string[] csv = new string[];
        int cnt = 0;
        // ヤマトの場合
        if (type === intYamato)
        {
            for (cnt = 0; cnt < values.Length; cnt++)
            {
                //お客様管理番号
                csv[cnt] = values[cnt][0].ToString();

                //送り状種類
                csv[cnt] += ',' + values[cnt][1].ToString();

                //クール区分
                csv[cnt] += ',' + values[cnt][2].ToString();

                //伝票番号
                csv[cnt] += ',' + values[cnt][3].ToString();

                //出荷予定日
                csv[cnt] += ',' + values[cnt][4].ToString();

                //お届け予定日
                csv[cnt] += ',' + values[cnt][5].ToString();

                //配達時間帯
                csv[cnt] += ',' + values[cnt][6].ToString();

                //お届け先コード
                csv[cnt] += ',' + values[cnt][7].ToString();

                //お届け先電話番号
                csv[cnt] += ',' + values[cnt][8].ToString();

                //お届け先電話番号枝番
                csv[cnt] += ',' + values[cnt][9].ToString();

                //お届け先郵便番号
                csv[cnt] += ',' + values[cnt][10].ToString();

                //お届け先住所
                csv[cnt] += ',' + values[cnt][11].ToString();

                //お届け先アパートマンション名
                csv[cnt] += ',' + values[cnt][12].ToString();

                //お届け先会社・部門１
                csv[cnt] += ',' + values[cnt][13].ToString();

                //お届け先会社・部門２
                csv[cnt] += ',' + values[cnt][14].ToString();

                //お届け先名
                csv[cnt] += ',' + values[cnt][15].ToString();

                //お届け先名(ｶﾅ)
                csv[cnt] += ',' + values[cnt][16].ToString();

                //敬称
                csv[cnt] += ',' + values[cnt][17].ToString();

                //ご依頼主コード
                csv[cnt] += ',' + values[cnt][18].ToString();

                //ご依頼主電話番号
                csv[cnt] += ',' + values[cnt][19].ToString();

                //ご依頼主電話番号枝番
                csv[cnt] += ',' + values[cnt][20].ToString();

                //ご依頼主郵便番号
                csv[cnt] += ',' + values[cnt][21].ToString();

                //ご依頼主住所
                csv[cnt] += ',' + values[cnt][22].ToString();

                //ご依頼主アパートマンション
                csv[cnt] += ',' + values[cnt][23].ToString();

                //ご依頼主名
                csv[cnt] += ',' + values[cnt][24].ToString();

                //ご依頼主名(ｶﾅ)
                csv[cnt] += ',' + values[cnt][25].ToString();

                //品名コード１
                csv[cnt] += ',' + values[cnt][26].ToString();

                //品名１
                csv[cnt] += ',' + values[cnt][27].ToString();

                //品名コード２
                csv[cnt] += ',' + values[cnt][28].ToString();

                //品名２
                csv[cnt] += ',' + values[cnt][29].ToString();

                //荷扱い１
                csv[cnt] += ',' + values[cnt][30].ToString();

                //荷扱い２
                csv[cnt] += ',' + values[cnt][31].ToString();

                //記事
                csv[cnt] += ',' + values[cnt][32].ToString();

                //ｺﾚｸﾄ代金引換額（税込)
                csv[cnt] += ',' + values[cnt][33].ToString();

                //内消費税額等
                csv[cnt] += ',' + values[cnt][34].ToString();

                //止置き
                csv[cnt] += ',' + values[cnt][35].ToString();

                //営業所コード
                csv[cnt] += ',' + values[cnt][36].ToString();

                //発行枚数
                csv[cnt] += ',' + values[cnt][37].ToString();

                //個数口表示フラグ
                csv[cnt] += ',' + values[cnt][38].ToString();

                //請求先顧客コード
                csv[cnt] += ',' + values[cnt][39].ToString();

                //請求先分類コード
                csv[cnt] += ',' + values[cnt][40].ToString();

                //運賃管理番号
                csv[cnt] += ',' + values[cnt][41].ToString();

                //クロネコwebコレクトデータ登録
                csv[cnt] += ',' + values[cnt][42].ToString();

                //クロネコwebコレクト加盟店番号
                csv[cnt] += ',' + values[cnt][43].ToString();

                //クロネコwebコレクト申込受付番号１
                csv[cnt] += ',' + values[cnt][44].ToString();

                //クロネコwebコレクト申込受付番号２
                csv[cnt] += ',' + values[cnt][45].ToString();

                //クロネコwebコレクト申込受付番号３
                csv[cnt] += ',' + values[cnt][46].ToString();

                //お届け予定ｅメール利用区分
                csv[cnt] += ',' + values[cnt][47].ToString();

                //お届け予定ｅメールe - mailアドレス
                csv[cnt] += ',' + values[cnt][48].ToString();

                //入力機種
                csv[cnt] += ',' + values[cnt][49].ToString();

                //お届け予定ｅメールメッセージ
                csv[cnt] += ',' + values[cnt][50].ToString();

                //お届け完了ｅメール利用区分
                csv[cnt] += ',' + values[cnt][51].ToString();

                //お届け完了ｅメールe - mailアドレス
                csv[cnt] += ',' + values[cnt][52].ToString();

                //お届け完了ｅメールメッセージ
                csv[cnt] += ',' + values[cnt][53].ToString();

                //クロネコ収納代行利用区分
                csv[cnt] += ',' + values[cnt][54].ToString();

                //予備
                csv[cnt] += ',' + values[cnt][55].ToString();

                //収納代行請求金額(税込)
                csv[cnt] += ',' + values[cnt][56].ToString();

                //収納代行内消費税額等
                csv[cnt] += ',' + values[cnt][57].ToString();

                //収納代行請求先郵便番号
                csv[cnt] += ',' + values[cnt][58].ToString();

                //収納代行請求先住所
                csv[cnt] += ',' + values[cnt][59].ToString();

                //収納代行請求先住所（アパートマンション名）
                csv[cnt] += ',' + values[cnt][60].ToString();

                //収納代行請求先会社・部門名１
                csv[cnt] += ',' + values[cnt][61].ToString();

                //収納代行請求先会社・部門名２
                csv[cnt] += ',' + values[cnt][62].ToString();

                //収納代行請求先名(漢字)
                csv[cnt] += ',' + values[cnt][63].ToString();

                //収納代行請求先名(カナ)
                csv[cnt] += ',' + values[cnt][64].ToString();

                //収納代行問合せ先名(漢字)
                csv[cnt] += ',' + values[cnt][65].ToString();

                //収納代行問合せ先郵便番号
                csv[cnt] += ',' + values[cnt][66].ToString();

                //収納代行問合せ先住所
                csv[cnt] += ',' + values[cnt][67].ToString();

                //収納代行問合せ先住所（アパートマンション名）
                csv[cnt] += ',' + values[cnt][68].ToString();

                //収納代行問合せ先電話番号
                csv[cnt] += ',' + values[cnt][69].ToString();

                //収納代行管理番号
                csv[cnt] += ',' + values[cnt][70].ToString();

                //収納代行品名
                csv[cnt] += ',' + values[cnt][71].ToString();

                //収納代行備考
                csv[cnt] += ',' + values[cnt][72].ToString();

                //複数口くくりキー
                csv[cnt] += ',' + values[cnt][73].ToString();

                //検索キータイトル1
                csv[cnt] += ',' + values[cnt][74].ToString();

                //検索キー1
                csv[cnt] += ',' + values[cnt][75].ToString();

                //検索キータイトル2
                csv[cnt] += ',' + values[cnt][76].ToString();

                //検索キー2
                csv[cnt] += ',' + values[cnt][77].ToString();

                //検索キータイトル3
                csv[cnt] += ',' + values[cnt][78].ToString();

                //検索キー3
                csv[cnt] += ',' + values[cnt][79].ToString();

                //検索キータイトル4
                csv[cnt] += ',' + values[cnt][80].ToString();

                //検索キー4
                csv[cnt] += ',' + values[cnt][81].ToString();

                //検索キータイトル5
                csv[cnt] += ',' + values[cnt][82].ToString();

                //検索キー5
                csv[cnt] += ',' + values[cnt][83].ToString();

                //予備
                csv[cnt] += ',' + values[cnt][84].ToString();

                //予備
                csv[cnt] += ',' + values[cnt][85].ToString();

                //投函予定メール利用区分
                csv[cnt] += ',' + values[cnt][86].ToString();

                //投函予定メールe - mailアドレス
                csv[cnt] += ',' + values[cnt][87].ToString();

                //投函予定メールメッセージ
                csv[cnt] += ',' + values[cnt][88].ToString();

                //投函完了メール（お届け先宛）利用区分
                csv[cnt] += ',' + values[cnt][89].ToString();

                //投函完了メール（お届け先宛）e - mailアドレス
                csv[cnt] += ',' + values[cnt][90].ToString();

                //投函完了メール（お届け先宛）メールメッセージ
                csv[cnt] += ',' + values[cnt][91].ToString();

                //投函完了メール（ご依頼主宛）利用区分
                csv[cnt] += ',' + values[cnt][92].ToString();

                //投函完了メール（ご依頼主宛）e - mailアドレス
                csv[cnt] += ',' + values[cnt][93].ToString();

                //投函完了メール（ご依頼主宛）メールメッセージ
                csv[cnt] += ',' + values[cnt][94].ToString();
            }
        }
        // 佐川の場合
        else if (type === intSagawa)
        {
            for (cnt = 0; cnt < values.Length; cnt++)
            {
                //住所録コード
                csv[cnt] = values[cnt][0].ToString();

                //お届け先電話番号
                csv[cnt] += ',' + values[cnt][1].ToString();

                //お届け先郵便番号
                csv[cnt] += ',' + values[cnt][2].ToString();

                //お届け先住所１（必須）
                csv[cnt] += ',' + values[cnt][3].ToString();

                //お届け先住所２
                csv[cnt] += ',' + values[cnt][4].ToString();

                //お届け先住所３
                csv[cnt] += ',' + values[cnt][5].ToString();

                //お届け先名称１（必須）
                csv[cnt] += ',' + values[cnt][6].ToString();

                //お届け先名称２
                csv[cnt] += ',' + values[cnt][7].ToString();

                //お客様管理ナンバー
                csv[cnt] += ',' + values[cnt][8].ToString();

                //お客様コード
                csv[cnt] += ',' + values[cnt][9].ToString();

                //部署・担当者
                csv[cnt] += ',' + values[cnt][10].ToString();

                //荷送人電話番号
                csv[cnt] += ',' + values[cnt][11].ToString();

                //ご依頼主電話番号
                csv[cnt] += ',' + values[cnt][12].ToString();

                //ご依頼主郵便番号
                csv[cnt] += ',' + values[cnt][13].ToString();

                //ご依頼主住所１
                csv[cnt] += ',' + values[cnt][14].ToString();

                //ご依頼主住所２
                csv[cnt] += ',' + values[cnt][15].ToString();

                //ご依頼主名称１
                csv[cnt] += ',' + values[cnt][16].ToString();

                //ご依頼主名称２
                csv[cnt] += ',' + values[cnt][17].ToString();

                //荷姿コード
                csv[cnt] += ',' + values[cnt][18].ToString();

                //品名１
                csv[cnt] += ',' + values[cnt][19].ToString();

                //品名２
                csv[cnt] += ',' + values[cnt][20].ToString();

                //品名３
                csv[cnt] += ',' + values[cnt][21].ToString();

                //品名４
                csv[cnt] += ',' + values[cnt][22].ToString();

                //品名５
                csv[cnt] += ',' + values[cnt][23].ToString();

                //出荷個数
                csv[cnt] += ',' + values[cnt][24].ToString();

                //便種（スピードで選択）
                csv[cnt] += ',' + values[cnt][25].ToString();

                //便種（商品）
                csv[cnt] += ',' + values[cnt][26].ToString();

                //配達日
                csv[cnt] += ',' + values[cnt][27].ToString();

                //配達指定時間帯
                csv[cnt] += ',' + values[cnt][28].ToString();

                //配達指定時間（時分）
                csv[cnt] += ',' + values[cnt][29].ToString();

                //代引金額
                csv[cnt] += ',' + values[cnt][30].ToString();

                //消費税
                csv[cnt] += ',' + values[cnt][31].ToString();

                //決済種別
                csv[cnt] += ',' + values[cnt][32].ToString();

                //保険金額
                csv[cnt] += ',' + values[cnt][33].ToString();

                //保険金額印字
                csv[cnt] += ',' + values[cnt][34].ToString();

                //指定シール①
                csv[cnt] += ',' + values[cnt][35].ToString();

                //指定シール②
                csv[cnt] += ',' + values[cnt][36].ToString();

                //指定シール③
                csv[cnt] += ',' + values[cnt][37].ToString();

                //営業店止め
                csv[cnt] += ',' + values[cnt][38].ToString();

                //ＳＲＣ区分
                csv[cnt] += ',' + values[cnt][39].ToString();

                //営業店コード
                csv[cnt] += ',' + values[cnt][40].ToString();

                //元着区分
                csv[cnt] += ',' + values[cnt][41].ToString();
            }
        }
        // 西濃の場合
        else if (type === intSeino)
        {
            for (cnt = 0; cnt < values.Length; cnt++)
            {
                //荷送人コード
                csv[cnt] = values[cnt][0].ToString();

                //西濃発店コード
                csv[cnt] += ',' + values[cnt][1].ToString();

                //出荷予定日
                csv[cnt] += ',' + values[cnt][2].ToString();

                //お問合せ番号
                csv[cnt] += ',' + values[cnt][3].ToString();

                //管理番号
                csv[cnt] += ',' + values[cnt][4].ToString();

                //元着区分
                csv[cnt] += ',' + values[cnt][5].ToString();

                //原票区分
                csv[cnt] += ',' + values[cnt][6].ToString();

                //個数
                csv[cnt] += ',' + values[cnt][7].ToString();

                //重量区分
                csv[cnt] += ',' + values[cnt][8].ToString();

                //重量（kg)
                csv[cnt] += ',' + values[cnt][9].ToString();

                //重量（才）
                csv[cnt] += ',' + values[cnt][10].ToString();

                //荷送人名称
                csv[cnt] += ',' + values[cnt][11].ToString();

                //荷送人住所１
                csv[cnt] += ',' + values[cnt][12].ToString();

                //荷送人住所２
                csv[cnt] += ',' + values[cnt][13].ToString();

                //荷送人電話番号
                csv[cnt] += ',' + values[cnt][14].ToString();

                //部署コード
                csv[cnt] += ',' + values[cnt][15].ToString();

                //部署名
                csv[cnt] += ',' + values[cnt][16].ToString();

                //重量契約区分
                csv[cnt] += ',' + values[cnt][17].ToString();

                //お届け先郵便番号
                csv[cnt] += ',' + values[cnt][18].ToString();

                //お届け先名称１
                csv[cnt] += ',' + values[cnt][19].ToString();

                //お届け先名称２
                csv[cnt] += ',' + values[cnt][20].ToString();

                //お届け先住所１
                csv[cnt] += ',' + values[cnt][21].ToString();

                //お届け先住所２
                csv[cnt] += ',' + values[cnt][22].ToString();

                //お届け先電話番号
                csv[cnt] += ',' + values[cnt][23].ToString();

                //お届け先コード
                csv[cnt] += ',' + values[cnt][24].ToString();

                //お届け先JIS市町村コード
                csv[cnt] += ',' + values[cnt][25].ToString();

                //着店コード付け区分
                csv[cnt] += ',' + values[cnt][26].ToString();

                //着地コード
                csv[cnt] += ',' + values[cnt][27].ToString();

                //着店コード
                csv[cnt] += ',' + values[cnt][28].ToString();

                //保険金額
                csv[cnt] += ',' + values[cnt][29].ToString();

                //輸送指示１
                csv[cnt] += ',' + values[cnt][30].ToString();

                //輸送指示２
                csv[cnt] += ',' + values[cnt][31].ToString();

                //記事１
                csv[cnt] += ',' + values[cnt][32].ToString();

                //記事２
                csv[cnt] += ',' + values[cnt][33].ToString();

                //記事３
                csv[cnt] += ',' + values[cnt][34].ToString();

                //記事４
                csv[cnt] += ',' + values[cnt][35].ToString();

                //記事５
                csv[cnt] += ',' + values[cnt][36].ToString();

                //輸送指示（配達指定日付）
                csv[cnt] += ',' + values[cnt][37].ToString();

                //輸送指示コード１
                csv[cnt] += ',' + values[cnt][38].ToString();

                //輸送指示コード２
                csv[cnt] += ',' + values[cnt][39].ToString();

                //輸送指示（止め店所名）
                csv[cnt] += ',' + values[cnt][40].ToString();

                //予備
                csv[cnt] += ',' + values[cnt][41].ToString();

                //品代金
                csv[cnt] += ',' + values[cnt][42].ToString();

                //消費税等
                csv[cnt] += ',' + values[cnt][43].ToString();

            }
        }

        return csv;
    } 
}